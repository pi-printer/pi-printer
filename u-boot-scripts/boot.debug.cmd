setenv ipaddr 192.168.1.112
setenv serverip 192.168.1.165
setenv bootargs console=ttyS0,115200 root=/dev/mmcblk0p1 rootwait rw panic=5
tftp 0x42000000 boot/uImage
tftp 0x43000000 boot/dtb
bootm 0x42000000 - 0x43000000
