use std::fmt::{Display, Formatter, Result};

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct NibbleArray(u32);

impl NibbleArray {
    fn set(self, idx: u8, val: u8) -> NibbleArray {
        NibbleArray(self.0 | ((val as u32) << (idx*4)))
    }

    pub fn from_f64(mut f: f64) -> NibbleArray {
        let mut res = NibbleArray(0);
        for i in 0..8 {
            f = (f - f.floor()) * 16.0;
            res = res.set(i, f.floor() as u8);
        }
        res
    }

    pub fn shiftl(self, n: u8) -> NibbleArray {
        NibbleArray(self.0 << (4*n))
    }

    pub fn compare(&self, other: &NibbleArray) -> u8 {
        let same = self.0 ^ other.0;
        (0u8..8).take_while(|i| same >> (i*4) == 0).count() as u8
    }
}

impl Display for NibbleArray {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{:08x}", self.0)
    }
}
