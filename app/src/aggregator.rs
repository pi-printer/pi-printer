use std::collections::BinaryHeap;
use std::sync::mpsc::Receiver;
use std::cmp::Reverse;

use crate::thread_pool::Response;

pub struct Aggregator {
    position: u64,
    results: BinaryHeap<Reverse<Response>>,
}

impl Aggregator {
    pub fn new(position: u64) -> Aggregator {
        Aggregator {
            position,
            results: BinaryHeap::new(),
        }
    }

    pub fn start(&mut self, rx: Receiver<Response>) {
        for res in rx {
            self.results.push(Reverse(res));
            self.aggregate();
        }
    }

    fn aggregate(&mut self) {
        // println!("aggregating heap of size {}", self.results.len());
        loop {
            match self.results.peek() {
                Some(Reverse(res)) if self.position == res.0 => {
                    let Reverse(res) = self.results.pop().unwrap();
                    self.position += 1;
                    self.process_response(res);
                },
                _ => break,
            }
        }
    }

    fn process_response(&mut self, Response(req, nibbles): Response){
        println!("req: {:?}, nibbles: {}", req, nibbles);
    }
}
