mod thread_pool;
mod aggregator;
mod scheduler;
mod pihex;
mod nibble_array;

use std::sync::mpsc::sync_channel;
use std::thread;

use crate::thread_pool::ThreadPool;
use crate::aggregator::Aggregator;

fn main() {
    let (tx, rx) = sync_channel(4);
    let start = 0;

    let mut a = Aggregator::new(start);
    let a_thread = thread::spawn(move || a.start(rx));

    {
        let p = ThreadPool::new(tx);
        for n in start.. {
            p.send(n).unwrap();
        }
    }

    a_thread.join().unwrap();
}
