use crate::nibble_array::NibbleArray;

const TERMS: [(u64, u64, f64); 7] = [
    (4, 1, -32.0),
    (4, 3, -1.0),
    (10, 1, 256.0),
    (10, 3, -64.0),
    (10, 5, -4.0),
    (10, 7, -4.0),
    (10, 9, 1.0),
];

fn modpow(mut base: u64, mut exp: u64, m: u64) -> u64 {
    base = base % m;
    let mut res = 1;
    while exp > 0 {
        if exp & 1 != 0 {
            res = (res * base) % m
        }
        base = (base * base) % m;
        exp >>= 1;
    }
    res
}

fn series_sum(d: u64, j: u64, k: u64) -> f64 {
    let m1_pow = |n| if n % 2 == 0 { 1.0 } else { -1.0 };

    let fraction1: f64 = (0..(2 * d + 2) / 5)
        .map(|i| {
            m1_pow(i) * modpow(4, 2 * d - 3 - 5 * i, j * i + k) as f64
                / (j * i + k) as f64
        })
        .fold(0.0, |x, y| (x + y).fract());
    let fraction2: f64 = ((2 * d + 2) / 5..)
        .map(|i| -(-4.0_f64).powi(-((5 * i + 3 - 2 * d) as i32)) / ((j * i + k) as f64))
        .take_while(|&x| x.abs() > 1e-13_f64)
        .sum();
    fraction1 + fraction2
}

pub fn pihex(d: u64) -> NibbleArray {
    NibbleArray::from_f64(
        TERMS.iter().map(|&(j, k, l)| l * series_sum(d, j, k)).sum()
    )
}
