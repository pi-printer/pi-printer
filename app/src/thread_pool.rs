use std::sync::mpsc::{sync_channel, SyncSender, TrySendError::{self, *}};
use std::thread;
use std::time::Duration;

use crate::nibble_array::NibbleArray;
use crate::pihex::pihex;

pub type Request = u64;
type Slave = SyncSender<Request>;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Response(pub Request, pub NibbleArray);

pub struct ThreadPool {
    slaves: Vec<Slave>,
}

impl ThreadPool {
    pub fn new(res_tx: SyncSender<Response>) -> ThreadPool {
        let slaves = (0..4)
            .map(move |_| {
                let (tx, rx) = sync_channel(4);
                let res_tx = res_tx.clone();
                thread::spawn(move || {
                    for req in rx {
                        let res = pihex(req);
                        res_tx.send(Response(req, res)).unwrap();
                    }
                });
                tx
            })
            .collect();

        ThreadPool { slaves }
    }

    pub fn send(&self, req: Request) -> Result<(), ()> {
        loop {
            match Self::send_impl(req, &self.slaves) {
                Ok(()) => return Ok(()),
                Err(Full(_)) => thread::sleep(Duration::from_millis(10)),
                Err(Disconnected(_)) => return Err(())
            }
        }
    }

    fn send_impl(req: Request, slaves: &[Slave]) -> Result<(), TrySendError<Request>> {
        let (s, ss) = slaves.split_first().ok_or(Full(req))?;
        match s.try_send(req) {
            Ok(())                 => Ok(()),
            Err(Full(req))         => Self::send_impl(req, ss),
            Err(Disconnected(req)) => Err(Disconnected(req)),
        }
    }
}
