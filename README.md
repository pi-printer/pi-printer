# pi-printer-linux

Linux OS purpose built for printing pi

![](https://media.giphy.com/media/26xBI73gWquCBBCDe/giphy.gif)

## Hardware support

This OS only supports the **Orange PI Zero** board.

Reasons:
 - has all the necessities
 - is cheap
 - has pi in the name
 - cheap
 - pretty cheap

No, I will not port it to Raspberry PI, raspberries are **disgusteng**.

![](https://media.giphy.com/media/3ohs7KViF6rA4aan5u/giphy.gif)

## Does it suck?

**Its only intention is to print, not suck!**

With that said, yeah... it does... at least until I make it work...

I should probably spend more time on building it rather than choosing gifs but meh...

## Information Sources

- [sunxi/Mainline Kernel Howto](https://linux-sunxi.org/Mainline_Kernel_Howto)
- [sunxi/Mainline U-Boot](https://linux-sunxi.org/Mainline_U-Boot)
- [kernel/Booting ARM Linux](https://www.kernel.org/doc/Documentation/arm/Booting)
